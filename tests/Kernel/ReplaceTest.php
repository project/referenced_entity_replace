<?php

namespace Drupal\Tests\referenced_entity_replace\Kernel;

use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\node\Traits\ContentTypeCreationTrait;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\language\Entity\ConfigurableLanguage;
use Drupal\referenced_entity_replace\ReplaceHelper;

/**
 * Tests ReplaceHelper.
 *
 * @group referenced_entity_replace
 */
class ReplaceTest extends KernelTestBase {

  use TaxonomyTestTrait;
  use NodeCreationTrait;
  use UserCreationTrait;
  use ContentTypeCreationTrait;
  use EntityReferenceTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'filter',
    'field',
    'language',
    'node',
    'taxonomy',
    'text',
    'user',
    'system',
    'referenced_entity_replace',
  ];

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  private $entityTypeManager;

  /**
   * A vocabulary used for testing.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  private $targetVocabulary;

  /**
   * A vocabulary used for testing.
   *
   * @var \Drupal\taxonomy\Entity\Vocabulary
   */
  private $referencingVocabulary;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->installConfig(['filter']);
    $this->installSchema('system', 'sequences');
    $this->installSchema('node', 'node_access');
    $this->installEntitySchema('entity_test_mul');
    $this->installEntitySchema('taxonomy_term');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('user');
    $this->installEntitySchema('node');
    $this->installConfig('node');
    $this->setUpContentType();
    $this->setUpReferencingVocabulary();
    $this->setUpUserReference();
    $this->setUpEntityTestMulReference();

    $this->entityTypeManager = \Drupal::entityTypeManager();
    $this->targetVocabulary = $this->createVocabulary();

    ConfigurableLanguage::createFromLangcode('en')->save();
    ConfigurableLanguage::createFromLangcode('fr')->save();
  }

  /**
   * Tests referencing entities are found.
   */
  public function testFindsReferences() {
    $term = $this->createTerm($this->targetVocabulary);
    $different_term = $this->createTerm($this->targetVocabulary);

    // Nodes that should be in results.
    $referencing_node = $this->createNode([
      'field_terms' => ['target_id' => $term->id()],
    ]);
    $also_referencing_node = $this->createNode([
      'field_also_terms' => ['target_id' => $term->id()],
    ]);
    $double_referencing_node = $this->createNode([
      'field_terms' => ['target_id' => $term->id()],
      'field_also_terms' => [
        ['target_id' => $different_term->id()],
        ['target_id' => $term->id()],
      ],
    ]);
    // Nodes that should not be in results.
    $this->createNode();
    $this->createNode([
      'field_terms' => ['target_id' => $different_term->id()],
    ]);
    // Term that should be in results.
    $referencing_term = $this->createTerm($this->referencingVocabulary, [
      'field_terms' => ['target_id' => $term->id()]
    ]);
    // Term that should not be in results.
    $this->createTerm($this->referencingVocabulary);
    // User that should be in results.
    $referencing_user = $this->createUser([], NULL, FALSE, [
      'field_terms' => ['target_id' => $term->id()],
    ]);
    // User that should not be in results.
    $this->createUser([], NULL, FALSE, [
      'field_terms' => ['target_id' => $different_term->id()],
    ]);
    // Translated entity referencing in original.
    $trans_en_ref = $this->createEntityTestMul(['target_id' => $term->id()]);
    $this->translateEntityTestMul($trans_en_ref, ['target_id' => $different_term->id()]);
    // Translated entity referencing in translation.
    $trans_fr_ref = $this->createEntityTestMul(['target_id' => $different_term->id()]);
    $this->translateEntityTestMul($trans_fr_ref, [
      ['target_id' => $different_term->id()],
      ['target_id' => $term->id()],
    ]);

    // Finding fields that could reference.
    $helper = $this->container->get('referenced_entity_replace.helper');
    assert($helper instanceof ReplaceHelper);
    $fields = $helper->getReferenceFields('taxonomy_term');
    $this->assertCount(5, $fields);
    $this->assertContains(['entity_type_id' => 'node', 'field_name' => 'field_terms'], $fields);
    $this->assertContains(['entity_type_id' => 'node', 'field_name' => 'field_also_terms'], $fields);
    $this->assertContains(['entity_type_id' => 'taxonomy_term', 'field_name' => 'field_terms'], $fields);
    $this->assertContains(['entity_type_id' => 'user', 'field_name' => 'field_terms'], $fields);
    $this->assertContains(['entity_type_id' => 'entity_test_mul', 'field_name' => 'field_translatable'], $fields);

    // Counting number of referencing entities.
    $this->assertEquals(3,
      $helper->getReferencingEntityIdsCount('node', ['field_terms', 'field_also_terms'], $term->id())
    );
    $this->assertEquals(1,
      $helper->getReferencingEntityIdsCount('user', ['field_terms'], $term->id())
    );
    $this->assertEquals(1,
      $helper->getReferencingEntityIdsCount('taxonomy_term', ['field_terms'], $term->id())
    );

    // Returning IDs for referencing Entities.
    $node_ids = $helper->getReferencingEntityIds('node', ['field_terms', 'field_also_terms'], $term->id());
    $this->assertCount(3, $node_ids);
    $this->assertContains($referencing_node->id(), $node_ids);
    $this->assertContains($also_referencing_node->id(), $node_ids);
    $this->assertContains($double_referencing_node->id(), $node_ids);
    $term_ids = $helper->getReferencingEntityIds('taxonomy_term', ['field_terms'], $term->id());
    $this->assertCount(1, $term_ids);
    $this->assertContains($referencing_term->id(), $term_ids);
    $user_ids = $helper->getReferencingEntityIds('user', ['field_terms'], $term->id());
    $this->assertCount(1, $user_ids);
    $this->assertContains($referencing_user->id(), $user_ids);
    $mul_ids = $helper->getReferencingEntityIds('entity_test_mul', ['field_translatable'], $term->id());
    $this->assertCount(2, $mul_ids);
    $this->assertContains($trans_en_ref->id(), $mul_ids);
    $this->assertContains($trans_fr_ref->id(), $mul_ids);
    // Check limit argument.
    $this->assertCount(2,
      $helper->getReferencingEntityIds('node', ['field_terms', 'field_also_terms'], $term->id(), 2)
    );
  }

  /**
   * Tests updating the references on entities.
   */
  public function testUpdateReferences() {
    $from_term = $this->createTerm($this->targetVocabulary);
    $to_term = $this->createTerm($this->targetVocabulary);
    $other_term = $this->createTerm($this->targetVocabulary);

    // Straight forward case. One value. To be changed.
    $referencing_node = $this->createNode([
      'field_terms' => ['target_id' => $from_term->id()],
    ]);
    // Two fields. One with value to be changed. Other to be left.
    $also_referencing_node = $this->createNode([
      'field_terms' => ['target_id' => $other_term->id()],
      'field_also_terms' => ['target_id' => $from_term->id()],
    ]);
    // Two fields. Both to change. One multi-valued.
    $double_referencing_node = $this->createNode([
      'field_terms' => ['target_id' => $from_term->id()],
      'field_also_terms' => [
        ['target_id' => $other_term->id()],
        ['target_id' => $from_term->id()],
      ],
    ]);
    // Including already referencing new value.
    $existing_referencing_node = $this->createNode([
      'field_also_terms' => [
        ['target_id' => $to_term->id()],
        ['target_id' => $from_term->id()],
        ['target_id' => $other_term->id()],
      ],
    ]);

    $helper = $this->container->get('referenced_entity_replace.helper');
    assert($helper instanceof ReplaceHelper);
    $this->assertCount(4,
      $helper->updateReferencingEntities(
        'node',
        [$referencing_node->id(), $also_referencing_node->id(), $double_referencing_node->id(), $existing_referencing_node->id()],
        ['field_terms', 'field_also_terms'],
        $to_term->id(),
        $from_term->id()
      )
    );

    $node_storage = $this->entityTypeManager->getStorage('node');
    $node_storage->resetCache();
    $referencing_node = $node_storage->load($referencing_node->id());
    $this->assertEquals($to_term->id(), $referencing_node->field_terms->target_id);
    $also_referencing_node = $node_storage->load($also_referencing_node->id());
    $this->assertEquals($other_term->id(), $also_referencing_node->field_terms->target_id);
    $this->assertEquals($to_term->id(), $also_referencing_node->field_also_terms->target_id);
    $double_referencing_node = $node_storage->load($double_referencing_node->id());
    $this->assertEquals($to_term->id(), $double_referencing_node->field_terms->target_id);
    $multiple_value = $double_referencing_node->field_also_terms->getValue();
    $this->assertCount(2, $multiple_value);
    $this->assertContains(['target_id' => $other_term->id()], $multiple_value);
    $this->assertContains(['target_id' => $to_term->id()], $multiple_value);
    $existing_referencing_node = $node_storage->load($existing_referencing_node->id());
    $multiple_value = $existing_referencing_node->field_also_terms->getValue();
    $this->assertCount(2, $multiple_value);
    $this->assertContains(['target_id' => $other_term->id()], $multiple_value);
    $this->assertContains(['target_id' => $to_term->id()], $multiple_value);

    // Translated entity referencing in original.
    $trans_en_ref = $this->createEntityTestMul(['target_id' => $from_term->id()]);
    $this->translateEntityTestMul($trans_en_ref, ['target_id' => $other_term->id()]);
    // Translated entity referencing in translation.
    $trans_fr_ref = $this->createEntityTestMul(['target_id' => $other_term->id()]);
    $this->translateEntityTestMul($trans_fr_ref, [
      ['target_id' => $other_term->id()],
      ['target_id' => $from_term->id()],
    ]);

    $this->assertCount(2,
      $helper->updateReferencingEntities(
        'entity_test_mul',
        [$trans_en_ref->id(), $trans_fr_ref->id()],
        ['field_translatable'],
        $to_term->id(),
        $from_term->id()
      )
    );

    $entity_test_storage = $this->entityTypeManager->getStorage('entity_test_mul');
    $entity_test_storage->resetCache();
    $trans_en_ref = $entity_test_storage->load($trans_en_ref->id());
    assert($trans_en_ref instanceof TranslatableInterface);
    $this->assertEquals($to_term->id(), $trans_en_ref->field_translatable->target_id);
    $trans_en_ref_fr = $trans_en_ref->getTranslation('fr');
    $this->assertEquals($other_term->id(), $trans_en_ref_fr->field_translatable->target_id);
    $trans_fr_ref = $entity_test_storage->load($trans_fr_ref->id());
    assert($trans_fr_ref instanceof TranslatableInterface);
    $this->assertEquals($other_term->id(), $trans_fr_ref->field_translatable->target_id);
    $trans_fr_ref_fr = $trans_fr_ref->getTranslation('fr');
    $multiple_value = $trans_fr_ref_fr->field_translatable->getValue();
    $this->assertCount(2, $multiple_value);
    $this->assertContains(['target_id' => $other_term->id()], $multiple_value);
    $this->assertContains(['target_id' => $to_term->id()], $multiple_value);
  }

  /**
   * Set up a content type for testing purposes.
   */
  private function setUpContentType() {
    $bundle = 'page';
    $this->createContentType([
      'type' => $bundle,
      'name' => 'Basic page',
      'display_submitted' => FALSE,
    ]);

    $this->createEntityReferenceField(
      entity_type: 'node',
      bundle: 'page',
      field_name: 'field_terms',
      field_label: 'field_terms',
      target_entity_type: 'taxonomy_term'
    );
    $this->createEntityReferenceField(
      entity_type: 'node',
      bundle: 'page',
      field_name: 'field_also_terms',
      field_label: 'field_also_terms',
      target_entity_type: 'taxonomy_term',
      selection_handler: 'default',
      selection_handler_settings: [],
      cardinality: 5
    );
  }

  /**
   * Add a reference field to user entity type.
   */
  private function setUpUserReference() {
    $this->createEntityReferenceField(
      entity_type: 'user',
      bundle: 'user',
      field_name: 'field_terms',
      field_label: 'field_terms',
      target_entity_type: 'taxonomy_term'
    );
  }

  /**
   * Set up a vocabulary for testing purposes.
   */
  private function setUpReferencingVocabulary() {
    $this->referencingVocabulary = $this->createVocabulary();

    $this->createEntityReferenceField(
      entity_type: 'taxonomy_term',
      bundle: $this->referencingVocabulary->id(),
      field_name: 'field_terms',
      field_label: 'field_terms',
      target_entity_type: 'taxonomy_term'
    );
  }

  /**
   * Set up reference field for multilingual test entity.
   */
  private function setUpEntityTestMulReference() {
    $this->createEntityReferenceField(
      entity_type: 'entity_test_mul',
      bundle: 'entity_test_mul',
      field_name: 'field_translatable',
      field_label: 'field_translatable',
      target_entity_type: 'taxonomy_term',
      selection_handler: 'default',
      selection_handler_settings: [],
      cardinality: FieldStorageConfig::CARDINALITY_UNLIMITED
    );
  }

  /**
   * Create entity_test_mul entity.
   */
  private function createEntityTestMul($field_translatable_value, $langcode = 'en'): TranslatableInterface {
    $entity = $this->entityTypeManager->getStorage('entity_test_mul')->create([
      'langcode' => $langcode,
      'field_translatable' => $field_translatable_value,
    ]);
    $entity->save();
    return $entity;
  }

  /**
   * Translate entity_test_mul entity.
   */
  private function translateEntityTestMul($entity, $field_translatable_value = NULL, $langcode = 'fr'): TranslatableInterface {
    if ($field_translatable_value) {
      $trans = $entity->addTranslation($langcode, [
        'field_translatable' => $field_translatable_value,
      ]);
    }
    else {
      $trans = $entity->addTranslation($langcode);
    }
    $trans->save();
    return $trans;
  }


}

