<?php

namespace Drupal\referenced_entity_replace\Drush\Commands;

use Drupal\referenced_entity_replace\BatchReplace;
use Drupal\referenced_entity_replace\ReplaceHelper;
use Drush\Commands\DrushCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A Drush commandfile.
 */
class ReferencedEntityReplaceCommands extends DrushCommands {

  /**
   * Constructs a ReferencedEntityReplaceCommands object.
   *
   * @param \Drupal\referenced_entity_replace\ReplaceHelper $helper
   *   The replace helper service.
   */
  public function __construct(
    protected ReplaceHelper $helper
  ) {
    parent::__construct();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('referenced_entity_replace.helper')
    );
  }

  /**
   * Update all references to an entity to another and remove.
   *
   * @param $entity_type_id
   *   Type of entity type.
   * @param $new_entity_id
   *   The entity to reference.
   * @param $old_entity_id
   *   The entity that is referenced to be removed.
   * @param array $options
   *   An associative array of options whose values come from cli, aliases, config, etc.
   * @option batch-size
   *   Number of entities to update per batch.
   * @option no-delete-old
   *   Set if you don't want to remove the old entity.
   * @usage referenced_entity_replace taxonomy_term 5 6 --batch-size=5
   *   Updates references to term 6 to 5 and removes term 6.
   *
   * @command referenced_entity_replace
   */
  public function replaceReferences($entity_type_id, $new_entity_id, $old_entity_id, $options = ['batch-size' => 10, 'no-delete-old' => FALSE]) {
    $fields = $this->helper->getReferenceFields($entity_type_id);
    if (!empty($fields)) {
      $batch_helper = new BatchReplace($entity_type_id, $new_entity_id, $old_entity_id, (int) $options['batch-size'], (bool) !$options['no-delete-old']);
      $batch = $batch_helper->buildReferenceEntityBatch($fields);
      \batch_set($batch->toArray());
      \drush_backend_batch_process();
    }
    else {
      $this->logger()->success(dt('No entities referencing %type %id.'));
    }
  }

}
