<?php

namespace Drupal\referenced_entity_replace;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\TranslatableInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\field\FieldStorageConfigInterface;

/**
 * Helper service for finding and updating entities with a particular entity reference.
 */
final class ReplaceHelper {

  use StringTranslationTrait;

  /**
   * Constructs a Merge object.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger channel factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(protected LoggerChannelFactoryInterface $logger, protected EntityTypeManagerInterface $entityTypeManager) {
  }

  /**
   * List fields that could reference the entity to be removed.
   *
   * @param string $entity_type_id
   *   The entity type being referenced.
   *
   * @return array
   *   Entity type and field name arrays referencing the entity type.
   */
  public function getReferenceFields(string $entity_type_id): array {
    $reference_fields = [];

    $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadMultiple();
    foreach ($fields as $field_name => $field) {
      assert ($field instanceof FieldStorageConfigInterface);
      // The field is entity reference and the target is of the same type as
      // the entity we want to merge.
      if ($field->getType() == 'entity_reference' && $field->getSetting('target_type') == $entity_type_id) {
        $reference_fields[] = ['entity_type_id' => $field->getTargetEntityTypeId(), 'field_name' => $field->getName()];
      }
    }

    return $reference_fields;
  }

  /**
   * Get count of entities that are referencing the ID.
   *
   * @param string $entity_type_id
   *   The entity type with the entity reference field.
   * @param string[] $field_names
   *   The machine name(s) of the entity reference fields on the entity type.
   * @param int|string $entity_id
   *   The entity ID to find entities referencing.
   *
   * @return int
   *   The total number of entities referencing the ID.
   */
  public function getReferencingEntityIdsCount(string $entity_type_id, array $field_names, string|int $entity_id): int {
    return $this->getReferencingEntityIdsQuery($entity_type_id, $field_names, $entity_id)->count()->execute();
  }

  /**
   * Get IDs for entities of type that are referencing the ID.
   *
   * @param string $entity_type_id
   *   The entity type with the entity reference field.
   * @param string[] $field_names
   *   The machine name(s) of the entity reference fields on the entity type.
   * @param int|string $entity_id
   *   The entity ID to find entities referencing.
   * @param int $limit
   *   Optional limit the number of results returned.
   *
   * @return array
   *   Array of IDs of entities with the reference.
    */
  public function getReferencingEntityIds(string $entity_type_id, array $field_names, string|int $entity_id, $limit = 0): array {
    $query = $this->getReferencingEntityIdsQuery($entity_type_id, $field_names, $entity_id);
    if ($limit) {
      $query->range(0, $limit);
    }
    return $query->execute();
  }

  /**
   * Get query for entity IDs that reference the entity to be removed.
   *
   * @param string $entity_type_id
   *   The entity type with the entity reference field.
   * @param string[] $field_names
   *   The machine name(s) of the entity reference fields on the entity type.
   * @param int|string $entity_id
   *   The entity ID to find entities referencing.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query.
   */
  public function getReferencingEntityIdsQuery(string $entity_type_id, array $field_names, string|int $entity_id): QueryInterface {
    $query = $this->entityTypeManager->getStorage($entity_type_id)->getQuery('OR');
    foreach ($field_names as $name) {
      $query->condition($name, $entity_id);
    }
    $query->accessCheck(FALSE);
    return $query;
  }

  /**
   * Update references on an entity.
   *
   * @param string entity_type_id
   *   The type of the entities with the reference fields to be updated.
   * @param array $entity_ids
   *   The IDs of the entities with the reference fields to be updated.
   * @param string[] $field_names
   *   The reference fields to be updated.
   * @param int|string $new_id
   *   The entity ID to which to change the references.
   * @param int|string $old_id
   *   The entity ID from which to change the references.
   *
   * @return array
   *   The IDs of entities updated.
   */
  public function updateReferencingEntities(string $entity_type_id, array $entity_ids, array $field_names, string|int $new_id, string|int $old_id): array {
    $updated_ids = [];

    $entities = $this->entityTypeManager->getStorage($entity_type_id)->loadMultiple($entity_ids);
    foreach ($entities as $entity) {
      assert($entity instanceof FieldableEntityInterface);
      if ($this->updateFieldItems($entity, $field_names, $new_id, $old_id)) {
        $entity->save();
        $updated_ids[$entity->id()] = $entity->id();
      }
      if ($entity instanceof TranslatableInterface) {
        foreach ($entity->getTranslationLanguages(FALSE) as $language) {
          $translation = $entity->getTranslation($language->getId());
          assert($translation instanceof FieldableEntityInterface);
          if ($this->updateFieldItems($translation, $field_names, $new_id, $old_id)) {
            $updated_ids[$entity->id()] = $entity->id();
            $translation->save();
          }
        }
      }
    }

    return $updated_ids;
  }

  /**
   * Update old references to new references.
   *
   * Iterate over all $field_names.
   * Check if it is targetting the old ID.
   * Check that it isn't already referencing the new ID.
   * Update the item, or remove it as required.
   */
  private function updateFieldItems(FieldableEntityInterface $entity, array $field_names, int|string $new_id, int|string $old_id): bool {
    $updated = FALSE;

    foreach ($field_names as $field_name) {
      foreach ($entity->get($field_name) as $delta => $field_item) {
        if ($field_item->target_id == $old_id) {
          $item_value = $field_item->getValue();
          // By updating the target_id property fields extending entity
          // reference with added properties will probably also work.
          $item_value['target_id'] = $new_id;
          // Prevent duplicate items.
          // Check that the item_value to be created doesn't already exist.
          if (array_search($item_value, $entity->get($field_name)->getValue()) === FALSE) {
            $entity->get($field_name)->set($delta, $item_value);
            $updated = TRUE;
          }
          else {
            // If it exists already, only remove this instance.
            $entity->get($field_name)->removeItem($delta);
            $updated = TRUE;
          }
        }
      }
    }

    return $updated;
  }

}
