<?php

namespace Drupal\referenced_entity_replace;

use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Batch builder and callbacks for updating entity reference fields.
 */
class BatchReplace {

  use StringTranslationTrait;

  /**
   * Constructs the BatchReplace object.
   *
   * @param string $entityTypeId
   *   The type of the entity being referenced.
   * @param string|int $newId
   *   The entity ID to which to change the references.
   * @param string|int $oldId
   *   The entity ID from which to change the references.
   * @param int $batchSize
   *   The number of entities with references to update in a batch. Default 10.
   * @param bool $deleteOld
   *   If the $oldId entity should be deleted on successful update of all references.
   */
  public function __construct(protected string $entityTypeId, protected string|int $newId, protected string|int $oldId, protected int $batchSize = 10, protected bool $deleteOld = TRUE) {
  }

  /**
   * Create a BatchBuilder object to run update reference operations.
   *
   * @see \Drupal\referenced_entity_replace\ReplaceHelper::getReferenceFields.
   *
   * @param array $fields
   *   An array of ['entity_type_id', 'field_name'] to be updated.
   *
   * @return \Drupal\Core\Batch\BatchBuilder
   *   The configured batch builder.
   */
  public function buildReferenceEntityBatch(array $fields): BatchBuilder {
    $batch_builder = new BatchBuilder();

    $entity_types = [];
    foreach ($fields as $field) {
      $entity_types[$field['entity_type_id']][] = $field['field_name'];
    }

    foreach ($entity_types as $entity_type_id => $field_names) {
      $batch_builder->addOperation([$this, 'batchUpdateReferencingEntities'], [
        $entity_type_id,
        $field_names,
      ]);
    }

    $batch_builder
      ->setTitle($this->t('Updating references in @fields fields', ['@fields' => count($fields)]))
      ->setFinishCallback([$this, 'batchUpdateReferencesFinished'])
      ->setErrorMessage($this->t('Batch has encountered an error'));

    return $batch_builder;
  }

  /**
   * Batch operation callback to update Entity References.
   *
   * @param string $entity_type_id
   *   The entity type with entity reference fields to be updated.
   * @param array $field_names
   *   The machine names of the entity reference fields.
   * @param $context
   *   The batch context.
   */
  public function batchUpdateReferencingEntities(string $entity_type_id, array $field_names, &$context): void {
    // Can't inject the service as this class is serialized for the batch process.
    $helper = \Drupal::service('referenced_entity_replace.helper');
    assert ($helper instanceof ReplaceHelper);

    // First time running operation. Count items to process.
    // Skip if there are none.
    if (empty($context['sandbox']['total'])) {
      $context['sandbox']['total'] = $helper->getReferencingEntityIdsCount($entity_type_id, $field_names, $this->oldId);
      if ($context['sandbox']['total'] == 0) {
        $context['finished'] = 1;
        $context['message'] = $this->t("No @type %field_names references for @old_id to update", [
          '@type' => $entity_type_id,
          '%field_names' => implode(', ', $field_names),
          '@old_id' => $this->oldId,
        ]);
        return;
      }
    }

    // Load batch of items, update them and the batch.
    $entity_ids = $helper->getReferencingEntityIds($entity_type_id, $field_names, $this->oldId, $this->batchSize);
    if (!empty($entity_ids)) {
      $helper->updateReferencingEntities($entity_type_id, $entity_ids, $field_names, $this->newId, $this->oldId);
      $context['sandbox']['processed'] = ($context['sandbox']['processed'] ?? 0) + count($entity_ids);
      $context['finished'] = $context['sandbox']['processed'] / $context['sandbox']['total'];
    }
    else {
      $context['finished'] = 1;
    }
    if ($context['finished'] == 1) {
      $context['message'] = $this->t("Updated @count @type %field_names references from @old_id to @new_id", [
        '@count' => $context['sandbox']['processed'],
        '@type' => $entity_type_id,
        '%field_names' => implode(', ', $field_names),
        '@old_id' => $this->oldId,
        '@new_id' => $this->newId,
      ]);
    }
  }

  /**
   * Batch operation complete callback.
   */
  public function batchUpdateReferencesFinished($success, $results, $operations): void {
    if ($success) {
      if ($this->deleteOld) {
        \Drupal::messenger()->addStatus($this->t('Removed %entity_type_id @id.', [
          '%entity_type_id' => $this->entityTypeId,
          '@id' => $this->oldId,
        ]));
        $old_entity = \Drupal::entityTypeManager()->getStorage($this->entityTypeId)->load($this->oldId);
        $old_entity->delete();
      }
      else {
        \Drupal::messenger()->addStatus($this->t('No delete option enabled %entity_type_id @id remains.', [
          '%entity_type_id' => $this->entityTypeId,
          '@id' => $this->oldId,
        ]));
      }
    }
    else {
      \Drupal::messenger()->addStatus($this->t('Batch operation error updating references. Not removed old %entity_type_id @id.', [
        '%entity_type_id' => $this->entityTypeId,
        '@id' => $this->oldId,
      ]));
    }
  }

}
